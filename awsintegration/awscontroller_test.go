package awsintegration

import "testing"

func TestCreateAwsController(t *testing.T) {
	ctrl := NewAwsController()

	if !ctrl.Verify() {
		t.Fail()
	}
}

func TestCreateAutoScalingGroup(t *testing.T) {
	ctrl := NewAwsController()
	if err := ctrl.CreateAutoScalingGroup("testgroup", 0); err != nil {
		t.Error(err)
	}
}
