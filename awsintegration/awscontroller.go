package awsintegration

import (
	"errors"
	"log"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/logger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/autoscaling"
	"github.com/aws/aws-sdk-go/service/ec2"
)

type AwsController struct {
	AutoscalingGroupName string
	awsConfig            *aws.Config
	ec2                  *ec2.EC2
	autoscaling          *autoscaling.AutoScaling
	Logger               logger.Logger
}

type AutScalingGroupConfig struct {
	LaunchConfigName string
	ImageId          string
	InstanceType     string
	VpcId            string
}

const DefaultAutoScalingGroupname = "kubernetes-nodes"
const DefaultRegion = "eu-west-1"

func NewAwsController(groupName string, logger logger.Logger) *AwsController {

	region := DefaultRegion

	config := &aws.Config{Region: &region}

	svc := ec2.New(config)
	autoscaling := autoscaling.New(config)

	return &AwsController{
		awsConfig:            config,
		ec2:                  svc,
		autoscaling:          autoscaling,
		AutoscalingGroupName: groupName,
		Logger:               logger,
	}
}

func (awsc *AwsController) Verify() bool {
	resp, err := awsc.ec2.DescribeInstances(nil)
	if err != nil {
		log.Println(err)
	}

	return len(resp.Reservations) > 0
}

func (awsc *AwsController) NrOfRunningMachines() (int, error) {
	resp, err := awsc.ec2.DescribeInstances(nil)
	if err != nil {
		return 0, err
	}

	return len(resp.Reservations), nil
}

func (awsc *AwsController) CreateAutoScalingGroup(config AutScalingGroupConfig) error {
	launchConfigName := "Default-Kubernetes-Node"
	imageId := "ami-6395b814"
	instanceType := "t2.small"

	nodeNames := []*string{&launchConfigName}

	out, err := awsc.autoscaling.DescribeLaunchConfigurations(&autoscaling.DescribeLaunchConfigurationsInput{LaunchConfigurationNames: nodeNames})
	if err != nil {
		log.Println(err)
	}

	if len(out.LaunchConfigurations) == 0 {
		_, err := awsc.autoscaling.CreateLaunchConfiguration(&autoscaling.CreateLaunchConfigurationInput{
			ImageId:                 &imageId,
			LaunchConfigurationName: &launchConfigName,
			InstanceType:            &instanceType,
		})

		if err != nil {
			return err
		}
	}

	var min int64 = 0
	var max int64 = 10
	vpcId := "subnet-45770820"

	_, err = awsc.autoscaling.CreateAutoScalingGroup(&autoscaling.CreateAutoScalingGroupInput{
		AutoScalingGroupName:    &awsc.AutoscalingGroupName,
		DesiredCapacity:         &min,
		MaxSize:                 &max,
		MinSize:                 &min,
		LaunchConfigurationName: &launchConfigName,
		VPCZoneIdentifier:       &vpcId,
	})

	return err
}

func (awsc *AwsController) SetNewClusterSize(nrOfMachines int) {

	var nr int64 = int64(nrOfMachines)

	awsc.autoscaling.SetDesiredCapacity(&autoscaling.SetDesiredCapacityInput{
		AutoScalingGroupName: &awsc.AutoscalingGroupName,
		DesiredCapacity:      &nr,
	})
}

func (awsc *AwsController) Decommission(node string) error {
	decrement := true

	input := autoscaling.DetachInstancesInput{
		AutoScalingGroupName:           &awsc.AutoscalingGroupName,
		InstanceIds:                    []*string{&node},
		ShouldDecrementDesiredCapacity: &decrement,
	}

	if _, err := awsc.autoscaling.DetachInstances(&input); err != nil {
		return err
	}

	instanceIds := []*string{&node}
	if _, err := awsc.ec2.TerminateInstances(&ec2.TerminateInstancesInput{InstanceIds: instanceIds}); err != nil {
		awsc.Logger.Printf("Error terminating instance %v: %v\n", node, err)
		return err
	}

	return nil
}

func (awsc *AwsController) FindInstanceId(nodeId string) (string, error) {
	output, err := awsc.ec2.DescribeInstances(&ec2.DescribeInstancesInput{})
	if err != nil {
		awsc.Logger.Println(err)
		return "", err
	}

	for _, reservation := range output.Reservations {
		for _, instance := range reservation.Instances {
			if *instance.PrivateDnsName == nodeId {
				return *instance.InstanceId, nil
			}
		}
	}

	return "", errors.New("No instance found for node " + nodeId)
}

func (awsc *AwsController) AddInstances(nrOfInstances int) error {
	describeInput := autoscaling.DescribeAutoScalingGroupsInput{
		AutoScalingGroupNames: []*string{&awsc.AutoscalingGroupName},
	}

	description, err := awsc.autoscaling.DescribeAutoScalingGroups(&describeInput)
	if err != nil {
		awsc.Logger.Println(err)
		awsc.Logger.Println("Could not find current desired capacity, aborting...")
		return err
	}

	if len(description.AutoScalingGroups) == 0 {
		awsc.Logger.Printf("AutoScaling group '%v' not found, aborting...\n", awsc.AutoscalingGroupName)
		return err
	}

	currentDesiredCapacity := description.AutoScalingGroups[0].DesiredCapacity
	newDesiredCapacity := *currentDesiredCapacity + int64(nrOfInstances)

	update := autoscaling.UpdateAutoScalingGroupInput{
		AutoScalingGroupName: &awsc.AutoscalingGroupName,
		DesiredCapacity:      &newDesiredCapacity,
	}

	if _, err = awsc.autoscaling.UpdateAutoScalingGroup(&update); err != nil {
		awsc.Logger.Println("Error updating capacity")
		awsc.Logger.Println(err)
		return err
	}

	awsc.Logger.Println("Updated cluster capacity")

	return nil
}
