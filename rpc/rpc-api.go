package rpc

import "bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"

type ScaleAppRequest struct {
	AppScaleTemplate scalingapi.AppScaleTemplate
}

type NodeInfoRequest struct {
}

type NodeInfoResponse struct {
	ClusterInfo scalingapi.ClusterInfo
	Err         string
}

type GenericNodeIdRequest struct {
	NodeId string
}

type DecommissionNodeRequest struct {
	NodeId string
	Force  bool
}

type DecommissionNodeResponse struct {
	InstanceIdShutDown string
	Err                string
}

type GenericResponse struct {
	Err string
}

type ScaleInstancesRequest struct {
	NrOfInstances int
}

type StoreScheduleRequest struct {
	Schedule scalingapi.Schedule
}

type ListStoredSchedulesRequest struct {
}

type ListStoredSchedulesResponse struct {
	Schedules []scalingapi.Schedule
}

type FindReplicationControllersRequest struct {
	Namespace string
	App       string
}

type FindReplicationControllersResponse struct {
	ReplicationControllers []string
	Err                    string
}
