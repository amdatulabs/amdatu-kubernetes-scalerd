package scalingapi_test

import (
	"encoding/json"
	"testing"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
)

var jsonInput = `
{
  "name": "nighttime",
  "cron": "0 0 22 * * *",
  "description": "Switch to half capacity at night",
  "desiredCapacity": 2,
  "appScaleTemplates": [
    {
      "app": "todo-demonstrator",
      "replicas": 1
    },
    {
      "app": "other-component",
      "replicas": 1
    }
  ]
}
`

func TestParseScheduleJSON(t *testing.T) {
	var schedule = scalingapi.Schedule{}
	json.Unmarshal([]byte(jsonInput), &schedule)

	if schedule.Name != "nighttime" {
		t.Error("Name not correctly set")
	}

	if schedule.Cron != "0 0 22 * * *" {
		t.Error("Cron not correctly set")
	}

	if schedule.Description != "Switch to half capacity at night" {
		t.Error("Description not correctly set")
	}

	if schedule.DesiredCapacity != 2 {
		t.Error("Capacity not correctly set")
	}

	if len(schedule.AppScaleTemplates) != 2 {
		t.Error("AppScaleTemplates not correctly set")
	}

	if schedule.AppScaleTemplates[0].App != "todo-demonstrator" {
		t.Error("AppScaleTemplate name not correctly set")
	}

	if schedule.AppScaleTemplates[0].Replicas != 1 {
		t.Error("AppScaleTemplate replicas not correctly set")
	}
}
