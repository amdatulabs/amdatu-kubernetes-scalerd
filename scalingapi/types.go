package scalingapi

import "bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/v1"

type ClusterInfo struct {
	Nodes       []v1.Node
	TotalMemory string
	TotalCPU    string
	Pods        map[string][]v1.Pod
}

type Schedule struct {
	Name              string             `json:"name"`
	Cron              string             `json:"cron"`
	Description       string             `json:"description"`
	DesiredCapacity   int                `json:"desiredCapacity"`
	AppScaleTemplates []AppScaleTemplate `json:"appScaleTemplates"`
}

type AppScaleTemplate struct {
	Namespace                           string                               `json:"namespace"`
	App                                 string                               `json:"app"`
	ReplicationControllerScaleTemplates []ReplicationControllerScaleTemplate `json:"replicationControllerScaleTemplates"`
}

type ReplicationControllerScaleTemplate struct {
	ReplicationController string `json:"replicationController"`
	Replicas              int    `json:"replicas"`
}
