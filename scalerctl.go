package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/rpc"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
	"github.com/codegangsta/cli"
	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
	"golang.org/x/net/context"
)

type Config struct {
	ScalerdUrl string
}

var config = Config{}
var ctx context.Context

func main() {
	app := cli.NewApp()
	app.Name = "scalerctl"
	app.Author = "Paul Bakker"
	app.Email = "paul.bakker@luminis.eu"
	app.Version = "0.0.1"

	app.Commands = []cli.Command{
		{
			Name:  "configure",
			Usage: "configure scalerctl",
			Action: func(c *cli.Context) {
				configure()
			},
		},
		{
			Name:  "get",
			Usage: "get information about the cluster",
			Action: func(c *cli.Context) {
				if len(c.Args()) != 1 {
					fmt.Println("Missing argument")
				}
				switch c.Args().First() {
				case "nodes":
					printNodeInfo()
					break
				case "schedules":
					printSchedules()
					break
				}
			},
		},
		{
			Name:  "shutdown",
			Usage: "drain node and shut down instance",
			Action: func(c *cli.Context) {
				nodeId := c.Args().First()
				if nodeId == "" {
					fmt.Println("No nodeid provided")
					os.Exit(1)
				}

				drain(nodeId, c.Bool("force"))
			},
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  "force",
					Usage: "Force action without graceful waits",
				},
			},
		},
		{
			Name:  "enable",
			Usage: "enable scheduling for a node",
			Action: func(c *cli.Context) {
				nodeId := c.Args().First()
				enable(nodeId)
			},
		},
		{
			Name:  "addinstances",
			Usage: "Add instances to the cluster",
			Action: func(c *cli.Context) {
				nrOfInstances, err := strconv.Atoi(c.Args().First())
				if err != nil {
					fmt.Println("Invalid number of instances provided")
					os.Exit(1)
				}

				addInstances(nrOfInstances)
			},
		},
		{
			Name:  "removeinstances",
			Usage: "Remove instances from the cluster",
			Action: func(c *cli.Context) {
				nrOfInstances, err := strconv.Atoi(c.Args().First())
				if err != nil {
					fmt.Println("Invalid number of instances provided")
					os.Exit(1)
				}

				removeInstances(nrOfInstances)
			},
		},
		{
			Name:  "create",
			Usage: "Schedule a scaling activity",
			Action: func(c *cli.Context) {
				inputFile := c.Args().First()
				if inputFile == "" {
					fmt.Print("No input file specified")
					os.Exit(1)
				}

				schedule := parseScheduleFile(inputFile)
				storeSchedule(schedule)
			},
		},
		{
			Name:  "scale-app",
			Usage: "Scale an app",
			Action: func(c *cli.Context) {

				// replicas is optional
				if len(c.Args()) < 2 || len(c.Args()) > 3 {
					fmt.Print("Invalid number of arguments. Provide a namespace, application name, and optionally the number of replicas, e.g. scalerctl scale-app mynamespace myapp [replicas]")
					os.Exit(1)
				}

				namespace := c.Args().Get(0)
				app := c.Args().Get(1)
				var replicas *int
				if len(c.Args()) == 3 {
					i, err := strconv.Atoi(c.Args().Get(2))
					if err != nil {
						fmt.Print("Invalid number of replicas")
						os.Exit(1)
					}
					replicas = &i
				}

				scaleApp(namespace, app, replicas)
			},
		},
	}

	if len(os.Args) > 1 && os.Args[1] != "configure" {
		readConfig()
		fmt.Printf("Configured to Scalerd API on %v\n", config.ScalerdUrl)
	}

	ctx = context.Background()

	app.Run(os.Args)
}

func scaleApp(namespace, app string, replicas *int) {

	// get available replication controllers
	proxy := proxymw{ctx, makeFindReplicationControllersProxyEndpoint(ctx)}
	controllers, err := proxy.findReplicationControllers(namespace, app)
	if err != nil {
		fmt.Println("Error getting replication controllers: " + err.Error())
		return
	}
	if len(controllers) == 0 {
		fmt.Println("No replication controllers found.")
		return
	}

	replicationControllerTemplates := []scalingapi.ReplicationControllerScaleTemplate{}

	if len(controllers) == 1 && replicas != nil {
		// if there is only 1 rc and we have the number of replicas, we are ready to go
		replicationControllerTemplates = append(replicationControllerTemplates, scalingapi.ReplicationControllerScaleTemplate{controllers[0], *replicas})
	} else {
		// ask user for nr of replicas for each controller
		for _, ctrl := range controllers {
			fmt.Print("Enter number of replicas for " + ctrl + ": ")
			var input string
			fmt.Scanln(&input)
			nr, err := strconv.Atoi(input)
			for err != nil {
				fmt.Print("Invalid number: " + err.Error() + "\nTry again, enter number of replicas for " + ctrl + ": ")
				fmt.Scanln(&input)
				nr, err = strconv.Atoi(input)
			}
			replicationControllerTemplates = append(replicationControllerTemplates, scalingapi.ReplicationControllerScaleTemplate{ctrl, nr})
		}
	}

	appScaleTemplate := scalingapi.AppScaleTemplate{namespace, app, replicationControllerTemplates}

	proxy = proxymw{ctx, makeScaleAppProxyEndpoint(ctx)}
	err = proxy.scaleApp(appScaleTemplate)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Could not scale app")
		os.Exit(1)
	}

}

func printNodeInfo() {

	proxy := proxymw{ctx, makeNodeInfoProxyEndpoint(ctx)}

	clusterInfo, err := proxy.getNodeInfo()
	if err != nil {
		fmt.Println(err)
		fmt.Println("Could not get cluster info")
		os.Exit(1)
	}

	fmt.Printf("Number of nodes: %v\n", len(clusterInfo.Nodes))
	fmt.Printf("Total CPU: %v\n", clusterInfo.TotalCPU)
	fmt.Printf("Total Memory: %v\n", clusterInfo.TotalMemory)
	fmt.Print("\n")

	for _, node := range clusterInfo.Nodes {
		fmt.Printf("Node: %v - CPU: %v - Memory: %v\n", node.Name, node.Status.Capacity["cpu"], node.Status.Capacity["memory"])
		fmt.Printf("Ready: %v\n", node.Status.Conditions[0].Status)
		fmt.Printf("Allows scheduling: %v\n", !node.Spec.Unschedulable)

		pods := clusterInfo.Pods[node.Name]

		if pods != nil && len(clusterInfo.Pods[node.Name]) > 0 {
			fmt.Println("Pods running on node:")
			for i, pod := range pods {
				fmt.Printf(" #%v %v\n", i, pod.Name)
			}
		} else {
			fmt.Println("Pods running on node: None")
		}

		fmt.Println("")
	}
}

func printSchedules() {
	proxy := proxymw{ctx, makeScheduleListProxyEndpoint(ctx)}
	proxy.listSchedules()

}

func drain(nodeId string, force bool) {
	proxy := proxymw{ctx, makeDecommissionNodeProxyEndpoint(ctx)}
	instanceId, err := proxy.decommissionNode(nodeId, force)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Printf("Node %v drained, and instance %v shut down", nodeId, instanceId)
}

func enable(nodeId string) {
	proxy := proxymw{ctx, makeEnableNodesProxyEndpoint(ctx)}
	err := proxy.enableNode(nodeId)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func addInstances(nrOfInstances int) {
	proxy := proxymw{ctx, makeAddInstancesProxyEndpoint(ctx)}
	err := proxy.addInstances(nrOfInstances)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func removeInstances(nrOfInstances int) {
	proxy := proxymw{ctx, makeRemoveInstancesProxyEndpoint(ctx)}
	err := proxy.removeInstances(nrOfInstances)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func configure() {
	scalerdurl := readLine("scalerd url")

	config := Config{
		ScalerdUrl: scalerdurl,
	}

	bytes, err := json.MarshalIndent(config, "", "   ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	_, err = os.Create(getConfigFile())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if err := ioutil.WriteFile(getConfigFile(), bytes, os.ModePerm); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func parseScheduleFile(inputFile string) scalingapi.Schedule {
	file, e := ioutil.ReadFile(inputFile)
	if e != nil {
		fmt.Printf("Input file could not be read: %v\n", e)
		os.Exit(1)
	}

	var schedule = scalingapi.Schedule{}
	json.Unmarshal(file, &schedule)

	return schedule
}

func storeSchedule(schedule scalingapi.Schedule) {
	proxy := proxymw{ctx, makeAddScheduleProxyEndpoint(ctx)}

	if err := proxy.addSchedule(schedule); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func readConfig() {
	bytes, err := ioutil.ReadFile(getConfigFile())

	if err != nil {
		fmt.Println(err)
		fmt.Println("Couldn't read config file, run 'scalerctl configure' first")
		os.Exit(1)
	}

	if err = json.Unmarshal(bytes, &config); err != nil {
		fmt.Println(err)
		fmt.Println("Couldn't read config file, run 'scalerctl configure' first")
		os.Exit(1)
	}
}

func getConfigFile() string {
	return path.Join(getHomeDir(), ".scalerctl")
}

func readLine(question string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("%v: ", question)
	text, _ := reader.ReadString('\n')
	return strings.Trim(text, "\n")
}

func getHomeDir() string {
	user, _ := user.Current()
	return user.HomeDir
}

func (mw proxymw) scaleApp(template scalingapi.AppScaleTemplate) error {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.ScaleAppRequest{template})
	if err != nil {
		return err
	}
	resp := response.(rpc.GenericResponse)
	if resp.Err != "" {
		return errors.New(resp.Err)
	}
	return nil
}

func (mw proxymw) findReplicationControllers(namespace, app string) ([]string, error) {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.FindReplicationControllersRequest{Namespace: namespace, App: app})
	if err != nil {
		return []string{}, err
	}

	resp := response.(rpc.FindReplicationControllersResponse)
	if resp.Err != "" {
		return resp.ReplicationControllers, errors.New(resp.Err)
	}
	return resp.ReplicationControllers, nil
}

func (mw proxymw) getNodeInfo() (scalingapi.ClusterInfo, error) {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.NodeInfoRequest{})
	if err != nil {
		return scalingapi.ClusterInfo{}, err
	}
	resp := response.(rpc.NodeInfoResponse)
	if resp.Err != "" {
		return resp.ClusterInfo, errors.New(resp.Err)
	}
	return resp.ClusterInfo, nil
}

func (mw proxymw) decommissionNode(nodeId string, force bool) (string, error) {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.DecommissionNodeRequest{nodeId, force})
	if err != nil {
		return "", err
	}
	resp := response.(rpc.DecommissionNodeResponse)
	if resp.Err != "" {
		return resp.InstanceIdShutDown, errors.New(resp.Err)
	}
	return resp.InstanceIdShutDown, nil
}

func (mw proxymw) enableNode(nodeId string) error {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.GenericNodeIdRequest{nodeId})
	if err != nil {
		return err
	}
	resp := response.(rpc.GenericResponse)
	if resp.Err != "" {
		return errors.New(resp.Err)
	}
	return nil
}

func (mw proxymw) addInstances(nrOfInstances int) error {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.ScaleInstancesRequest{nrOfInstances})
	if err != nil {
		return err
	}
	resp := response.(rpc.GenericResponse)
	if resp.Err != "" {
		return errors.New(resp.Err)
	}
	return nil
}

func (mw proxymw) removeInstances(nrOfInstances int) error {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.ScaleInstancesRequest{nrOfInstances})
	if err != nil {
		return err
	}
	resp := response.(rpc.GenericResponse)
	if resp.Err != "" {
		return errors.New(resp.Err)
	}
	return nil
}

func (mw proxymw) addSchedule(schedule scalingapi.Schedule) error {
	fmt.Println(schedule.AppScaleTemplates)

	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.StoreScheduleRequest{schedule})
	if err != nil {
		return err
	}
	resp := response.(rpc.GenericResponse)
	if resp.Err != "" {
		return errors.New(resp.Err)
	}
	return nil
}

func (mw proxymw) listSchedules() error {
	response, err := mw.ScalingServiceEndpoint(mw.Context, rpc.ListStoredSchedulesRequest{})
	if err != nil {
		return err
	}
	resp := response.(rpc.ListStoredSchedulesResponse)

	bytes, err := json.MarshalIndent(resp.Schedules, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(bytes))

	return nil
}

type proxymw struct {
	context.Context
	ScalingServiceEndpoint endpoint.Endpoint
}

func makeScaleAppProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/scaleapp")

	return httptransport.NewClient(
		"POST",
		endpointUrl,
		encodeRequest,
		decodeGenericResponse,
	).Endpoint()
}

func makeFindReplicationControllersProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/findReplicationControllers")

	return httptransport.NewClient(
		"GET",
		endpointUrl,
		encodeRequest,
		decodeFindReplicatiomControllersResponse,
	).Endpoint()
}

func makeNodeInfoProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/nodes")

	return httptransport.NewClient(
		"GET",
		endpointUrl,
		encodeRequest,
		decodeNodeInfoResponse,
	).Endpoint()
}

func makeDecommissionNodeProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/decommission")

	return httptransport.NewClient(
		"POST",
		endpointUrl,
		encodeRequest,
		decodeDecommissionNodeResponse,
	).Endpoint()
}

func makeEnableNodesProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/enable")

	return httptransport.NewClient(
		"POST",
		endpointUrl,
		encodeRequest,
		decodeGenericResponse,
	).Endpoint()
}

func makeAddInstancesProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/addinstances")

	return httptransport.NewClient(
		"POST",
		endpointUrl,
		encodeRequest,
		decodeGenericResponse,
	).Endpoint()
}

func makeRemoveInstancesProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/removeinstances")

	return httptransport.NewClient(
		"POST",
		endpointUrl,
		encodeRequest,
		decodeGenericResponse,
	).Endpoint()
}

func makeAddScheduleProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpointUrl, _ := url.Parse(config.ScalerdUrl + "/schedules")

	return httptransport.NewClient(
		"POST",
		endpointUrl,
		encodeRequest,
		decodeGenericResponse,
	).Endpoint()
}

func makeScheduleListProxyEndpoint(ctx context.Context) endpoint.Endpoint {
	endpoindUrl, _ := url.Parse(config.ScalerdUrl + "/listschedules")
	return httptransport.NewClient(
		"POST",
		endpoindUrl,
		encodeRequest,
		decodeScheduleListResponse,
	).Endpoint()
}

func encodeRequest(r *http.Request, request interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeFindReplicatiomControllersResponse(r *http.Response) (interface{}, error) {
	var response rpc.FindReplicationControllersResponse
	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		return nil, err
	}
	return response, nil
}

func decodeNodeInfoResponse(r *http.Response) (interface{}, error) {
	var response rpc.NodeInfoResponse
	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		return nil, err
	}
	return response, nil
}

func decodeDecommissionNodeResponse(r *http.Response) (interface{}, error) {
	var response rpc.DecommissionNodeResponse
	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		return nil, err
	}
	return response, nil
}

func decodeGenericResponse(r *http.Response) (interface{}, error) {
	var response rpc.GenericResponse
	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		return nil, err
	}
	return response, nil
}

func decodeScheduleListResponse(r *http.Response) (interface{}, error) {
	var response rpc.ListStoredSchedulesResponse
	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		return nil, err
	}
	return response, nil
}
