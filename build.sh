#!/bin/bash

# this should be called within an alpine docker container during manual build OR from the bitbucket pipeline

# fail immedialtely
set -e
set -o pipefail

echo "Branch: $BITBUCKET_BRANCH"
echo "Tag: $BITBUCKET_TAG"
echo "Commit: $BITBUCKET_COMMIT"

BASE_NAME=amdatu-kubernetes
DAEMON_NAME="${BASE_NAME}-scalerd"
CTL_NAME="${BASE_NAME}-scalerctl"

ALPHA_LINUX_NAME_DAEMON="$DAEMON_NAME-linux_amd64-alpha"
ALPHA_MACOS_NAME_DAEMON="$DAEMON_NAME-macos_amd64-alpha"
ALPHA_WIN_NAME_DAEMON="$DAEMON_NAME-windows_amd64-alpha.exe"
HASHED_LINUX_NAME_DAEMON="$DAEMON_NAME-linux_amd64-${BITBUCKET_COMMIT}"
HASHED_MACOS_NAME_DAEMON="$DAEMON_NAME-macos_amd64-${BITBUCKET_COMMIT}"
HASHED_WIN_NAME_DAEMON="$DAEMON_NAME-windows_amd64-${BITBUCKET_COMMIT}.exe"
ALPHA_IMAGE_DAEMON="amdatu/${DAEMON_NAME}:alpha"
HASHED_IMAGE_DAEMON="amdatu/${DAEMON_NAME}:${BITBUCKET_COMMIT}"

ALPHA_LINUX_NAME_CTL="$CTL_NAME-linux_amd64-alpha"
ALPHA_MACOS_NAME_CTL="$CTL_NAME-macos_amd64-alpha"
ALPHA_WIN_NAME_CTL="$CTL_NAME-windows_amd64-alpha.exe"
HASHED_LINUX_NAME_CTL="$CTL_NAME-linux_amd64-${BITBUCKET_COMMIT}"
HASHED_MACOS_NAME_CTL="$CTL_NAME-macos_amd64-${BITBUCKET_COMMIT}"
HASHED_WIN_NAME_CTL="$CTL_NAME-windows_amd64-${BITBUCKET_COMMIT}.exe"
ALPHA_IMAGE_CTL="amdatu/${CTL_NAME}:alpha"
HASHED_IMAGE_CTL="amdatu/${CTL_NAME}:${BITBUCKET_COMMIT}"

if [ -z "$BITBUCKET_TAG" ]; then

    # create correct golang dir structure for this project
    PACKAGE_PATH="${GOPATH}/src/bitbucket.org/amdatulabs/${DAEMON_NAME}"
    mkdir -p "${PACKAGE_PATH}"

    # copy sources to new directory
    cd "${SRCDIR}"
    tar -cO --exclude .git . | tar -xv -C "${PACKAGE_PATH}"

    # build binaries
    echo "Building binaries"
    cd "${PACKAGE_PATH}"

    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o "$ALPHA_LINUX_NAME_DAEMON" ./scalerd.go
    CGO_ENABLED=0 GOOS=darwin go build -a -installsuffix cgo -o "$ALPHA_MACOS_NAME_DAEMON" ./scalerd.go
    CGO_ENABLED=0 GOOS=windows go build -a -installsuffix cgo -o "$ALPHA_WIN_NAME_DAEMON" ./scalerd.go
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o "$ALPHA_LINUX_NAME_CTL" ./scalerctl.go
    CGO_ENABLED=0 GOOS=darwin go build -a -installsuffix cgo -o "$ALPHA_MACOS_NAME_CTL" ./scalerctl.go
    CGO_ENABLED=0 GOOS=windows go build -a -installsuffix cgo -o "$ALPHA_WIN_NAME_CTL" ./scalerctl.go

    echo "Build successful"

fi

if [ -z "$BITBUCKET_TAG" ] && [ "$BITBUCKET_BRANCH" == "master" ]; then

    # publish alpha versions for master branch
    if [ -n "$BB_AUTH_STRING" ]; then

        # upload binaries to download section of bitbucket

        echo "Uploading binaries"
        cp "$ALPHA_LINUX_NAME_DAEMON" "$HASHED_LINUX_NAME_DAEMON"
        cp "$ALPHA_MACOS_NAME_DAEMON" "$HASHED_MACOS_NAME_DAEMON"
        cp "$ALPHA_WIN_NAME_DAEMON" "$HASHED_WIN_NAME_DAEMON"

        cp "$ALPHA_LINUX_NAME_CTL" "$HASHED_LINUX_NAME_CTL"
        cp "$ALPHA_MACOS_NAME_CTL" "$HASHED_MACOS_NAME_CTL"
        cp "$ALPHA_WIN_NAME_CTL" "$HASHED_WIN_NAME_CTL"

        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$ALPHA_LINUX_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$ALPHA_MACOS_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$ALPHA_WIN_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$HASHED_LINUX_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$HASHED_MACOS_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$HASHED_WIN_NAME_DAEMON"

        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$ALPHA_LINUX_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$ALPHA_MACOS_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$ALPHA_WIN_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$HASHED_LINUX_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$HASHED_MACOS_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$HASHED_WIN_NAME_CTL"
    else
        echo "No bitbucket auth token set, skipping upload"
    fi

    if [ -n "$DOCKER_USER" ] && [ -n "$DOCKER_MAIL" ] && [ -n "$DOCKER_PASSWORD" ]; then

        # build docker image for alpha version

        echo "Building alpha daemon docker image"
        cp "$ALPHA_LINUX_NAME_DAEMON" "$DAEMON_NAME"
        docker build -t "$ALPHA_IMAGE_DAEMON" -f Dockerfile-scalerd .
        echo "Tagging hashed daemon docker image"
        docker tag "$ALPHA_IMAGE_DAEMON" "$HASHED_IMAGE_DAEMON"

        echo "Building alpha ctl docker image"
        cp "$ALPHA_LINUX_NAME_CTL" "$CTL_NAME"
        docker build -t "$ALPHA_IMAGE_CTL" -f Dockerfile-scalerctl .
        echo "Tagging hashed ctl docker image"
        docker tag "$ALPHA_IMAGE_CTL" "$HASHED_IMAGE_CTL"

        echo "Pushing docker images"
        docker login --username="$DOCKER_USER" --email="$DOCKER_MAIL" --password="$DOCKER_PASSWORD"
        docker push "$ALPHA_IMAGE_DAEMON"
        docker push "$HASHED_IMAGE_DAEMON"
        docker push "$ALPHA_IMAGE_CTL"
        docker push "$HASHED_IMAGE_CTL"
    else
        echo "No docker user/password set, skipping image build and push"
    fi

elif [ -n "$BITBUCKET_TAG" ]; then

    # create binaries and images versioned with given git tag
    # use existing hashed versions, do not rebuild!

    if [ -n "$BB_AUTH_STRING" ]; then

        # download old hashed version, rename, upload

        echo "Downloading old binaries"
        curl -sL "https://bitbucket.org/amdatulabs/${APPNAME}/downloads/${HASHED_LINUX_NAME_DAEMON}" -o "$HASHED_LINUX_NAME_DAEMON"
        curl -sL "https://bitbucket.org/amdatulabs/${APPNAME}/downloads/${HASHED_MACOS_NAME_DAEMON}" -o "$HASHED_MACOS_NAME_DAEMON"
        curl -sL "https://bitbucket.org/amdatulabs/${APPNAME}/downloads/${HASHED_WIN_NAME_DAEMON}" -o "$HASHED_WIN_NAME_DAEMON"
        curl -sL "https://bitbucket.org/amdatulabs/${APPNAME}/downloads/${HASHED_LINUX_NAME_CTL}" -o "$HASHED_LINUX_NAME_CTL"
        curl -sL "https://bitbucket.org/amdatulabs/${APPNAME}/downloads/${HASHED_MACOS_NAME_CTL}" -o "$HASHED_MACOS_NAME_CTL"
        curl -sL "https://bitbucket.org/amdatulabs/${APPNAME}/downloads/${HASHED_WIN_NAME_CTL}" -o "$HASHED_WIN_NAME_CTL"

        TAGGED_LINUX_NAME_DAEMON="$DAEMON_NAME-linux_amd64-${BITBUCKET_TAG}"
        TAGGED_MACOS_NAME_DAEMON="$DAEMON_NAME-macos_amd64-${BITBUCKET_TAG}"
        TAGGED_WIN_NAME_DAEMON="$DAEMON_NAME-windows_amd64-${BITBUCKET_TAG}.exe"
        TAGGED_LINUX_NAME_CTL="$CTL_NAME-linux_amd64-${BITBUCKET_TAG}"
        TAGGED_MACOS_NAME_CTL="$CTL_NAME-macos_amd64-${BITBUCKET_TAG}"
        TAGGED_WIN_NAME_CTL="$CTL_NAME-windows_amd64-${BITBUCKET_TAG}.exe"

        cp "$HASHED_LINUX_NAME_DAEMON" "$TAGGED_LINUX_NAME_DAEMON"
        cp "$HASHED_MACOS_NAME_DAEMON" "$TAGGED_MACOS_NAME_DAEMON"
        cp "$HASHED_WIN_NAME_DAEMON" "$TAGGED_WIN_NAME_DAEMON"
        cp "$HASHED_LINUX_NAME_CTL" "$TAGGED_LINUX_NAME_CTL"
        cp "$HASHED_MACOS_NAME_CTL" "$TAGGED_MACOS_NAME_CTL"
        cp "$HASHED_WIN_NAME_CTL" "$TAGGED_WIN_NAME_CTL"

        echo "Uploading renamed binaries"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$TAGGED_LINUX_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$TAGGED_MACOS_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$TAGGED_WIN_NAME_DAEMON"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$TAGGED_LINUX_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$TAGGED_MACOS_NAME_CTL"
        curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$TAGGED_WIN_NAME_CTL"
    else
        echo "No bitbucket auth token set, skipping upload"
    fi

    if [ -n "$DOCKER_USER" ] && [ -n "$DOCKER_MAIL" ] && [ -n "$DOCKER_PASSWORD" ]; then

        # promoting alpha to version defined by tag name

        echo "Tagging docker images with $BITBUCKET_TAG"
        docker pull "$HASHED_IMAGE_DAEMON"
        TAGGED_IMAGE_DAEMON="amdatu/${DAEMON_NAME}:${BITBUCKET_TAG}"
        docker tag "$HASHED_IMAGE_DAEMON" "$TAGGED_IMAGE_DAEMON"

        docker pull "$HASHED_IMAGE_CTL"
        TAGGED_IMAGE_CTL="amdatu/${CTL_NAME}:${BITBUCKET_TAG}"
        docker tag "$HASHED_IMAGE_CTL" "$TAGGED_IMAGE_CTL"

        echo "Pushing docker images"
        docker login --username="$DOCKER_USER" --email="$DOCKER_MAIL" --password="$DOCKER_PASSWORD"
        docker push "$TAGGED_IMAGE_DAEMON"
        docker push "$TAGGED_IMAGE_CTL"

        if [ "$BITBUCKET_TAG" == "production" ]; then
            echo "Also tagging and pushing latest"
            LATEST_IMAGE_DAEMON="amdatu/${DAEMON_NAME}:latest"
            docker tag "$HASHED_IMAGE_DAEMON" "$LATEST_IMAGE_DAEMON"

            LATEST_IMAGE_CTL="amdatu/${CTL_NAME}:latest"
            docker tag "$HASHED_IMAGE_CTL" "$LATEST_IMAGE_CTL"

            docker push "$LATEST_IMAGE_DAEMON"
            docker push "$LATEST_IMAGE_CTL"
        fi
    else
        echo "No docker user/password set, skipping image tag and push"
    fi

else
    echo "Not on master branch, and not triggered by tag: skipping publishing"
fi

echo "Done"