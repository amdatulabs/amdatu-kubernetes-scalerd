package storage

import (
	"encoding/json"
	"log"
	"os"
	"strings"
	"testing"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
	etcdClient "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

var etcdUrl = "http://10.150.16.16:2379"
var storage *EtcdScheduleStorage
var kAPI etcdClient.KeysAPI

func TestMain(m *testing.M) {
	cfg := etcdClient.Config{
		Endpoints: []string{etcdUrl},
	}

	etcd, err := etcdClient.New(cfg)
	if err != nil {
		log.Fatal(err)
	}

	kAPI = etcdClient.NewKeysAPI(etcd)

	storage = NewEtcdScheduleStorage(etcd)

	os.Exit(m.Run())
}

func TestStore(t *testing.T) {
	storage.Clear()

	_, err := kAPI.Get(context.Background(), "/scalerd/schedules", &etcdClient.GetOptions{})
	if !strings.Contains(err.Error(), "Key not found") {
		t.Fatal(err)
	}

	storage.Store(scalingapi.Schedule{Name: "TestSchedule", Cron: "0 0 21 * * *", DesiredCapacity: 1})

	result, err := kAPI.Get(context.Background(), "/scalerd/schedules", &etcdClient.GetOptions{})
	if err != nil {
		t.Error(err)
	}

	if result.Node.Nodes.Len() != 1 {
		t.Errorf("Invalid numbers of nodes found: %v ", result.Node.Nodes.Len())
	}

	schedule := scalingapi.Schedule{}
	if err := json.Unmarshal([]byte(result.Node.Nodes[0].Value), &schedule); err != nil {
		t.Error("Error parsing json value", err)
	}

}

func TestList(t *testing.T) {
	storage.Clear()

	_, err := kAPI.Get(context.Background(), "/scalerd/schedules", &etcdClient.GetOptions{})
	if !strings.Contains(err.Error(), "Key not found") {
		t.Fatal(err)
	}

	storage.Store(scalingapi.Schedule{Name: "TestSchedule", Cron: "0 0 21 * * *", DesiredCapacity: 1})
	list, err := storage.List()
	if err != nil {
		t.Error(err)
	}

	if len(list) != 1 {
		t.Errorf("Invalid number of schedules found: %v", len(list))
	}

	if list[0].Name != "TestSchedule" {
		t.Error("Invalid schedule found")
	}

}
