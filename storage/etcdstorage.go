package storage

import (
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
	"github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

type EtcdScheduleStorage struct {
	etcdAPI client.KeysAPI
}

func NewEtcdScheduleStorage(etcdClient client.Client) *EtcdScheduleStorage {
	kAPI := client.NewKeysAPI(etcdClient)

	return &EtcdScheduleStorage{kAPI}
}

func (storage *EtcdScheduleStorage) Store(schedule scalingapi.Schedule) error {

	bytes, err := json.Marshal(schedule)
	if err != nil {
		log.Println(err)

		return err
	}

	if _, err := storage.etcdAPI.Set(context.Background(), "scalerd/schedules/"+schedule.Name, string(bytes), nil); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (storage *EtcdScheduleStorage) List() ([]scalingapi.Schedule, error) {

	result := []scalingapi.Schedule{}

	items, err := storage.etcdAPI.Get(context.Background(), "scalerd/schedules", &client.GetOptions{Recursive: true})
	if err != nil {
		return result, err
	}

	for _, item := range items.Node.Nodes {

		schedule := scalingapi.Schedule{}
		if err := json.Unmarshal([]byte(item.Value), &schedule); err != nil {
			fmt.Printf("Invalid JSON for key %v\n", item.Key)
			fmt.Println(err)
			continue
		}

		result = append(result, schedule)
	}

	return result, err
}

func (storage *EtcdScheduleStorage) Clear() error {
	if _, err := storage.etcdAPI.Delete(context.Background(), "scalerd/schedules", &client.DeleteOptions{Recursive: true, Dir: true}); err != nil {
		return err
	}

	return nil
}
