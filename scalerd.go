package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/awsintegration"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/kubernetes"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/logger"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/rpc"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/storage"
	etcdclient "github.com/coreos/etcd/client"
	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/robfig/cron"
	"golang.org/x/net/context"
)

var kubernetesurl, port, kubernetesUsername, kubernetesPassword, etcdUrl string
var awsc *awsintegration.AwsController
var k8Ctrl *kubernetes.KubernetesController
var sysoutlogger = logger.SysoutLogger{}
var scheduleStorage *storage.EtcdScheduleStorage
var cronScheduler *cron.Cron

func init() {
	flag.StringVar(&kubernetesurl, "kubernetes", "", "URL to the Kubernetes API server")
	flag.StringVar(&port, "httpport", "8100", "Port to listen")
	flag.StringVar(&kubernetesUsername, "kubernetesusername", "noauth", "Username to authenticate against Kubernetes API server. Skip authentication when not set")
	flag.StringVar(&kubernetesPassword, "kubernetespassword", "noauth", "Username to authenticate against Kubernetes API server.")
	flag.StringVar(&etcdUrl, "etcd", "", "etcd url")

	exampleUsage := "Missing required argument %v. Example usage: scalerd -kubernetes http://[kubernetes-api-url]:8080 -etcd http://[etcd-url]:2379 -httpport 8000"

	flag.Parse()

	if kubernetesurl == "" {
		log.Fatalf(exampleUsage, "kubernetes")
	}

}

func main() {
	cronScheduler = cron.New()
	cronScheduler.Start()

	log.Printf("Configured to Kubernetes API on %v\n", kubernetesurl)
	connectKubernetes()
	connectAws()

	cfg := etcdclient.Config{
		Endpoints: []string{etcdUrl},
	}

	etcdClient, err := etcdclient.New(cfg)
	if err != nil {
		log.Fatal("Couldn't connect to etcd")
	}

	scheduleStorage = storage.NewEtcdScheduleStorage(etcdClient)

	makeNodeInfoEndpoint()

	ctx := context.Background()
	scaleAppHandler := httptransport.NewServer(
		ctx,
		makeScaleAppEndpoint(),
		decodeScaleAppRequest,
		encodeResponse,
	)

	findReplicationControllersHandler := httptransport.NewServer(
		ctx,
		makeFindReplicationControllersEndpoint(),
		decodeFindReplicationControllersRequest,
		encodeResponse,
	)

	nodeInfoHandler := httptransport.NewServer(
		ctx,
		makeNodeInfoEndpoint(),
		decodeNodeInfoRequest,
		encodeResponse,
	)

	decommissionNodeHandler := httptransport.NewServer(
		ctx,
		makeDecommissionNodeEnpoint(),
		decodeDecommissionNodeRequest,
		encodeResponse,
	)

	enableNodesHandler := httptransport.NewServer(
		ctx,
		makeEnableNodesHandler(),
		decodeGenericNodeIdRequest,
		encodeResponse,
	)

	addInstancesHandler := httptransport.NewServer(
		ctx,
		makeAddInstancesHandler(),
		decodeScaleInstancesRequest,
		encodeResponse,
	)

	removeInstancesHandler := httptransport.NewServer(
		ctx,
		makeRemoveInstancesHandler(),
		decodeScaleInstancesRequest,
		encodeResponse,
	)

	addScheduleHandler := httptransport.NewServer(
		ctx,
		makeAddScheduleHandler(),
		decodeAddScheduleRequest,
		encodeResponse,
	)

	addListSchedulesHandler := httptransport.NewServer(
		ctx,
		makeListSchedulesHandler(),
		decodeListSchedulesRequest,
		encodeResponse,
	)

	getReplicationControllersHandler := httptransport.NewServer(
		ctx,
		makeGetReplicationControllers(),
		decodeGetReplicationControllersRequest,
		encodeResponse,
	)

	http.Handle("/scaleapp", scaleAppHandler)
	http.Handle("/findReplicationControllers", findReplicationControllersHandler)
	http.Handle("/nodes", nodeInfoHandler)
	http.Handle("/decommission", decommissionNodeHandler)
	http.Handle("/enable", enableNodesHandler)
	http.Handle("/addinstances", addInstancesHandler)
	http.Handle("/removeinstances", removeInstancesHandler)
	http.Handle("/schedules", addScheduleHandler)
	http.Handle("/listschedules", addListSchedulesHandler)
	http.Handle("/replicationcontrollers", getReplicationControllersHandler)

	loadScheduledScalingEvents()

	log.Printf("Listening on port %v...\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func connectKubernetes() {
	k8Ctrl = kubernetes.NewKubernetsController(kubernetesurl, kubernetesUsername, kubernetesPassword, sysoutlogger)
}

func connectAws() {
	groupName := awsintegration.DefaultAutoScalingGroupname

	awsc = awsintegration.NewAwsController(groupName, sysoutlogger)
}

func loadScheduledScalingEvents() {
	log.Println("Loading scheduled events from etcd")

	schedules, err := scheduleStorage.List()
	if err != nil {
		log.Println(err)
	}

	for _, schedule := range schedules {
		log.Printf("Recovering cron for schedule %v\n", schedule.Name)

		cronScheduler.AddFunc(schedule.Cron, func() {
			handleScalingEvent(&schedule)
		})
	}
}

func makeScaleAppEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.ScaleAppRequest).AppScaleTemplate
		err := k8Ctrl.ScaleApp(req)
		if err != nil {
			return rpc.GenericResponse{err.Error()}, nil
		}
		return rpc.GenericResponse{""}, nil
	}
}

func makeFindReplicationControllersEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.FindReplicationControllersRequest)
		controllers, err := k8Ctrl.FindReplicationControllersForApp(req.Namespace, req.App)
		if err != nil {
			return rpc.FindReplicationControllersResponse{controllers, err.Error()}, nil
		}
		return rpc.FindReplicationControllersResponse{controllers, ""}, nil
	}
}

func makeNodeInfoEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		info, err := k8Ctrl.GetClusterInfo()
		if err != nil {
			return rpc.NodeInfoResponse{info, err.Error()}, nil
		}
		return rpc.NodeInfoResponse{info, ""}, nil
	}
}

func makeDecommissionNodeEnpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.DecommissionNodeRequest)
		decomissionedInstance, err := decommissionNode(req.NodeId, req.Force)

		if err != nil {
			return rpc.DecommissionNodeResponse{"", err.Error()}, nil
		}

		return rpc.DecommissionNodeResponse{decomissionedInstance, ""}, nil
	}
}

func makeEnableNodesHandler() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.GenericNodeIdRequest)

		var err error
		if req.NodeId != "" {
			err = k8Ctrl.EnableNode(req.NodeId)
		} else {
			err = k8Ctrl.EnableAllNodes()
		}

		if err != nil {
			return rpc.GenericResponse{err.Error()}, nil
		}

		return rpc.GenericResponse{""}, nil
	}
}

func makeAddInstancesHandler() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.ScaleInstancesRequest)

		err := awsc.AddInstances(req.NrOfInstances)

		if err != nil {
			return rpc.GenericResponse{err.Error()}, nil
		}

		return rpc.GenericResponse{""}, nil
	}
}

func makeRemoveInstancesHandler() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.ScaleInstancesRequest)

		err := removeInstances(req.NrOfInstances)

		if err != nil {
			return rpc.GenericResponse{err.Error()}, nil
		}

		return rpc.GenericResponse{""}, nil
	}
}

func makeAddScheduleHandler() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(rpc.StoreScheduleRequest)

		fmt.Println(req.Schedule)
		if err := scheduleStorage.Store(req.Schedule); err != nil {
			return nil, err
		}

		cronScheduler.AddFunc(req.Schedule.Cron, func() {
			handleScalingEvent(&req.Schedule)
		})

		return rpc.GenericResponse{""}, nil
	}
}

func makeListSchedulesHandler() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		schedules, err := scheduleStorage.List()
		if err != nil {
			log.Println(err)
		}

		return rpc.ListStoredSchedulesResponse{schedules}, nil
	}
}

func makeGetReplicationControllers() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		req := request.(rpc.FindReplicationControllersRequest)
		controllers, err := k8Ctrl.FindReplicationControllersForApp(req.Namespace, req.App)

		if err != nil {
			return rpc.FindReplicationControllersResponse{controllers, err.Error()}, nil
		}

		return rpc.FindReplicationControllersResponse{controllers, ""}, nil
	}
}

func handleScalingEvent(schedule *scalingapi.Schedule) {
	log.Printf("Handling scaling event: %v\n", schedule.Name)

	log.Printf("Templates: %v", schedule.AppScaleTemplates)

	for _, template := range schedule.AppScaleTemplates {
		k8Ctrl.ScaleApp(template)
	}

	clusterInfo, err := k8Ctrl.GetClusterInfo()
	if err != nil {
		sysoutlogger.Println(err)
	}

	nrOfNodes := len(clusterInfo.Nodes)

	if nrOfNodes == schedule.DesiredCapacity {
		sysoutlogger.Println("Number of nodes already equals desired capacity, nothing more to do")
	} else if nrOfNodes > schedule.DesiredCapacity {
		scaleDownCluster(schedule.DesiredCapacity)
	} else {
		requiredNewNodes := schedule.DesiredCapacity - nrOfNodes

		sysoutlogger.Printf("Adding %v nodes to the cluster", requiredNewNodes)
		awsc.AddInstances(requiredNewNodes)
	}

	log.Printf("Completed scaling '%v'\n", schedule.Name)
}

func removeInstances(nrToRemove int) error {
	log.Printf("Removing %v instances\n", nrToRemove)

	clusterInfo, err := k8Ctrl.GetClusterInfo()
	if err != nil {
		sysoutlogger.Println(err)
		return err
	}

	nrOfNodes := len(clusterInfo.Nodes)

	if nrOfNodes < nrToRemove {
		sysoutlogger.Println("Can not remove more instances than are running")
		return errors.New("Can not remove more instances than are running")
	} else {
		newNrOfNodes := nrOfNodes - nrToRemove
		scaleDownCluster(newNrOfNodes)
	}

	log.Printf("Completed removing %v instances\n", nrToRemove)

	return nil
}

func scaleDownCluster(desiredNrOfNodes int) {
	shutdownNodes := k8Ctrl.ScaleDownCluster(desiredNrOfNodes)

	for _, node := range shutdownNodes {
		instanceId, err := awsc.FindInstanceId(node)
		if err != nil {
			log.Println(err)
			continue
		}

		if err := awsc.Decommission(instanceId); err != nil {
			log.Println(err)
		}
	}

}

func decodeScaleAppRequest(r *http.Request) (interface{}, error) {
	var request rpc.ScaleAppRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeFindReplicationControllersRequest(r *http.Request) (interface{}, error) {
	var request rpc.FindReplicationControllersRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeNodeInfoRequest(r *http.Request) (interface{}, error) {
	var request rpc.NodeInfoRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeGenericNodeIdRequest(r *http.Request) (interface{}, error) {
	var request rpc.GenericNodeIdRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeScaleInstancesRequest(r *http.Request) (interface{}, error) {
	var request rpc.ScaleInstancesRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeDecommissionNodeRequest(r *http.Request) (interface{}, error) {
	var request rpc.DecommissionNodeRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeAddScheduleRequest(r *http.Request) (interface{}, error) {
	var request rpc.StoreScheduleRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeListSchedulesRequest(r *http.Request) (interface{}, error) {
	var request rpc.ListStoredSchedulesRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeGetReplicationControllersRequest(r *http.Request) (interface{}, error) {
	var request rpc.FindReplicationControllersRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func decommissionNode(nodeId string, force bool) (string, error) {
	k8Ctrl.DecomissionNode(nodeId, force)

	instanceId, err := awsc.FindInstanceId(nodeId)
	if err != nil {
		log.Println(err)
		return "", err
	}

	if err := awsc.Decommission(instanceId); err != nil {
		log.Println(err)
		return "", err
	}

	fmt.Printf("Node %v drained, and instance %v shut down", nodeId, instanceId)
	return instanceId, nil
}
