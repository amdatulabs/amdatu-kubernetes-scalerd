package kubernetes

import (
	"testing"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/v1"
	k8sClient "bitbucket.org/amdatulabs/amdatu-kubernetes-go/client"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/logger"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
)

const kubernetesUrl = "http://10.150.16.32:8080"
const node1 = "ip-10-150-46-247.eu-west-1.compute.internal"
const node2 = "ip-10-150-26-35.eu-west-1.compute.internal"
const nodeName = node1

func TestEnableAllNodes(t *testing.T) {
	ctrl := createK8sController()

	ctrl.EnableAllNodes()

	nodes, err := ctrl.K8Client.ListNodes()
	if err != nil {
		t.Error(err)
	}

	for _, node := range nodes.Items {
		if node.Spec.Unschedulable == true {
			t.Fail()
		}
	}
}

func TestFindPodsOnNode(t *testing.T) {
	ctrl := createK8sController()

	pods, err := ctrl.FindPodsOnNode(nodeName)
	if err != nil {
		t.Error(err)
	}

	if len(pods) == 0 {
		t.Fail()
	}
}

func TestGetClusterInfo(t *testing.T) {
	ctrl := createK8sController()

	info, err := ctrl.GetClusterInfo()
	if err != nil {
		t.Error(err)
	}

	if len(info.Nodes) < 1 {
		t.Error("No nodes found")
	}
}

func TestDecommissionNode(t *testing.T) {
	ctrl := createK8sController()

	ctrl.DecomissionNode(nodeName, false)

	node, _ := ctrl.K8Client.GetNode(nodeName)

	if node.Spec.Unschedulable == false {
		t.Error("Node not set to unschedulable")
	}

	pods, _ := ctrl.FindPodsOnNode(nodeName)
	for _, pod := range pods {
		if len(pod.ObjectMeta.Labels) > 0 {
			t.Error("Pod still has labels")
		}
	}
}

func TestChooseRandomNodes(t *testing.T) {
	ctrl := createK8sController()
	nodes := []v1.Node{}
	for i := 0; i < 10; i++ {
		nodes = append(nodes, v1.Node{})
	}

	result := ctrl.randomlyChooseNodes(2, nodes)
	if len(result) != 2 {
		t.Error("Wrong number of nodes returned")
	}

}

func TestScaleApp(t *testing.T) {
	ctrl := createK8sController()

	rcTemplates := []scalingapi.ReplicationControllerScaleTemplate{scalingapi.ReplicationControllerScaleTemplate{"*", 2}}
	appTemplate := scalingapi.AppScaleTemplate{"default", "todo", rcTemplates}

	if err := ctrl.ScaleApp(appTemplate); err != nil {
		t.Error("Error scaling app:" + err.Error())
	}

	rcTemplates[0].Replicas = 1
	if err := ctrl.ScaleApp(appTemplate); err != nil {
		t.Error("Error scaling app:" + err.Error())
	}
}

func createK8sController() KubernetesController {
	return KubernetesController{K8Client: k8Connect(), Logger: logger.SysoutLogger{}}
}

func k8Connect() *k8sClient.Client {
	c := k8sClient.NewClient(kubernetesUrl, "", "")

	return &c
}
