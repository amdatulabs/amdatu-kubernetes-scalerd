package kubernetes

import (
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/amdatulabs/amdatu-kubernetes-deployer/healthcheck"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/resource"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/v1"
	k8sClient "bitbucket.org/amdatulabs/amdatu-kubernetes-go/client"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/logger"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-scalerd/scalingapi"
)

type KubernetesController struct {
	K8Client *k8sClient.Client
	Logger   logger.Logger
}

func NewKubernetsController(kubernetesUrl, kubernetesUsername, kubernetesPassword string, logger logger.Logger) *KubernetesController {
	c := k8sClient.NewClient(kubernetesUrl, kubernetesUsername, kubernetesPassword)

	return &KubernetesController{&c, logger}
}

func (k8Controller *KubernetesController) GetClusterInfo() (scalingapi.ClusterInfo, error) {
	nodes, err := k8Controller.K8Client.ListNodes()

	if err != nil {
		return scalingapi.ClusterInfo{}, err
	}

	clusterInfo := scalingapi.ClusterInfo{}
	clusterInfo.Nodes = nodes.Items

	var totalCPU, totalMemory *resource.Quantity
	clusterInfo.Pods = make(map[string][]v1.Pod)

	for i, node := range nodes.Items {
		cpu := node.Status.Capacity["cpu"]
		memory := node.Status.Capacity["memory"]

		if i == 0 {

			totalCPU = &cpu
			totalMemory = &memory
		} else {
			totalCPU.Add(cpu)
			totalMemory.Add(memory)
		}

		pods, _ := k8Controller.FindPodsOnNode(node.Name)
		podList := []v1.Pod{}
		for _, pod := range pods {
			podList = append(podList, *pod)
		}

		clusterInfo.Pods[node.Name] = podList
	}

	if len(nodes.Items) > 0 {
		clusterInfo.TotalCPU = totalCPU.String()
		clusterInfo.TotalMemory = totalMemory.String()
	}

	return clusterInfo, nil
}

func (k8Controller *KubernetesController) DecomissionNode(nodeName string, force bool) error {
	k8Controller.Logger.Printf("Decommissioning node %v, force = %v\n", nodeName, force)
	node, err := k8Controller.K8Client.GetNode(nodeName)
	if err != nil {
		k8Controller.Logger.Println(err)
		return err
	}

	node.Spec.Unschedulable = true
	k8Controller.K8Client.UpdateNode(node)

	pods, err := k8Controller.FindPodsOnNode(nodeName)
	if err != nil {
		k8Controller.Logger.Print(err)
		return err
	}

	if !force {
		for _, pod := range pods {
			k8Controller.decommissionPod(pod)
		}
	}

	if err = k8Controller.K8Client.DeleteNode(nodeName); err != nil {
		k8Controller.Logger.Println(err)
		return err
	}

	return nil
}

func (k8Controller *KubernetesController) ScaleDownCluster(desiredCapacity int) []string {
	nodeList, err := k8Controller.K8Client.ListNodes()
	if err != nil {
		k8Controller.Logger.Println("Error listing nodes: ", err)
	}

	nrOfNodesToShutdown := len(nodeList.Items) - desiredCapacity

	nodesToShutdown := k8Controller.randomlyChooseNodes(nrOfNodesToShutdown, nodeList.Items)

	shutdownNodes := []string{}

	for _, node := range nodesToShutdown {
		k8Controller.Logger.Printf("Shutting down node %v...\n", node.Name)
		err := k8Controller.DecomissionNode(node.Name, false)
		shutdownNodes = append(shutdownNodes, node.Name)

		if err != nil {
			k8Controller.Logger.Println(err)
			k8Controller.Logger.Printf("Error while shutting down node %v. Scaling down will continue, but the desired capacity may not be reached\nb", node.Name)
		}
	}

	return shutdownNodes
}

func (k8Controller *KubernetesController) randomlyChooseNodes(nrOfNodes int, nodes []v1.Node) []v1.Node {

	shuffleNodeList(nodes)

	return nodes[0:nrOfNodes]
}

func shuffleNodeList(nodes []v1.Node) {
	for i := range nodes {
		j := rand.Intn(i + 1)
		nodes[i], nodes[j] = nodes[j], nodes[i]
	}
}

func (k8Controller *KubernetesController) FindPodsOnNode(nodeName string) ([]*v1.Pod, error) {
	pods, err := k8Controller.K8Client.ListPods("")
	if err != nil {
		return nil, err
	}

	result := []*v1.Pod{}

	for i := range pods.Items {
		if pods.Items[i].Spec.NodeName == nodeName {
			result = append(result, &pods.Items[i])
		}
	}

	return result, nil
}

/**
Removes labels from the Pod so that the RC will forget about it and start a new one.
Before doing so, a check is done on /health. If this returns, health checks should be used.
A watch is created for the new Pod. When the new pod is started and healthy, the old pod can be removed.
*/
func (k8Controller *KubernetesController) decommissionPod(pod *v1.Pod) {

	if len(pod.ObjectMeta.Labels) == 0 {
		k8Controller.Logger.Printf("Pod %v doesn't have any labels, nothing to decomission\n", pod.Name)
		return
	}

	healthcheckAvailable := k8Controller.healthcheckAvailable(pod)
	var healthcheckAvailableString string
	if healthcheckAvailable {
		healthcheckAvailableString = "enabled"
	} else {
		healthcheckAvailableString = "disabled"
	}

	k8Controller.Logger.Printf("Healthchecks %v for %v\n", healthcheckAvailableString, pod.Name)
	origLabels := pod.ObjectMeta.Labels
	pod.ObjectMeta.Labels = make(map[string]string)

	podWatch, signals, err := k8Controller.K8Client.WatchPodsWithLabel(pod.Namespace, origLabels)
	k8Controller.K8Client.UpdatePod(pod.Namespace, pod)

	if err != nil {
		k8Controller.Logger.Println(err)
	}

	//Block until pod is started and healthy
	k8Controller.Logger.Println("Block until pods get healthy")
	for evt := range podWatch {
		podObj := evt.Object

		if podObj.Status.Phase == "Running" {
			k8Controller.Logger.Println("Found running pod")
			if healthcheckAvailable {
				url := fmt.Sprintf("http://%v:%v/%v", podObj.Status.PodIP, podObj.Spec.Containers[0].Ports[0], "health")
				if !healthcheck.WaitForPodStarted(url, 30*time.Second) {
					k8Controller.Logger.Println("Pod %v didn't become healthy!", pod.Name)
				}
			}

			break
		}
	}

	signals <- "CANCEL"

	k8Controller.Logger.Printf("Decomissioned Pod %v\n", pod.Name)

	k8Controller.K8Client.DeletePod(pod.Namespace, pod.Name)
}

func (k8Controller *KubernetesController) EnableAllNodes() error {
	nodes, err := k8Controller.K8Client.ListNodes()
	if err != nil {
		return err
	}

	for _, node := range nodes.Items {
		node.Spec.Unschedulable = false
		k8Controller.K8Client.UpdateNode(&node)
		k8Controller.Logger.Printf("Enable scheduling for node %v\n", node.Name)
	}

	return nil
}

func (k8Controller *KubernetesController) EnableNode(nodeId string) error {
	node, err := k8Controller.K8Client.GetNode(nodeId)
	if err != nil {
		return err
	}

	node.Spec.Unschedulable = false
	k8Controller.K8Client.UpdateNode(node)
	k8Controller.Logger.Printf("Enable scheduling for node %v\n", node.Name)

	return nil
}

func (k8Controller *KubernetesController) healthcheckAvailable(pod *v1.Pod) bool {

	ip := pod.Status.PodIP
	port := pod.Spec.Containers[0].Ports[0]

	url := fmt.Sprintf("http://%v:%v/health", ip, port.ContainerPort)
	k8Controller.Logger.Printf("Checking for a healthcheck on %v\n", url)

	resp, err := http.Get(url)
	if err != nil {
		return false
	}

	return resp.StatusCode != 404
}

func (k8Controller *KubernetesController) ScaleApp(template scalingapi.AppScaleTemplate) error {

	namespace := getOrDefaultNamespace(template.Namespace)

	rcLabelSelector := map[string]string{"app": template.App}
	availableControllers, err := k8Controller.K8Client.ListReplicationControllersWithLabel(namespace, rcLabelSelector)
	if err != nil {
		k8Controller.Logger.Printf("Error finding replication controllers for app %v: %v\n", template.App, err)
		return err
	}
	if len(availableControllers.Items) == 0 {
		k8Controller.Logger.Printf("No replication controller found for app %v\n", template.App)
		return errors.New("No replication controller found for app")
	}

	var scaleInfos []scaleInfo

	// first get all replication controllers to scale
	// if the first replication controller has the name "*", scale all RCs of the app
	if template.ReplicationControllerScaleTemplates[0].ReplicationController == "*" {
		k8Controller.Logger.Printf("Scaling all "+strconv.Itoa(len(availableControllers.Items))+" replication controllers for app %v\n", template.App)
		replicas := template.ReplicationControllerScaleTemplates[0].Replicas
		for _, ctrl := range availableControllers.Items {
			scaleInfos = append(scaleInfos, scaleInfo{ctrl, replicas})
		}
		// else check if given replication controllers exist
	} else {
		for _, rcTemplate := range template.ReplicationControllerScaleTemplates {
			ctrl, err := findReplicationControllerByName(availableControllers.Items, rcTemplate.ReplicationController)
			if err != nil {
				return err
			}
			scaleInfos = append(scaleInfos, scaleInfo{*ctrl, rcTemplate.Replicas})
		}
	}

	// now get the actual replicationcontrollers and scale them
	for i := range scaleInfos {
		ctrl := scaleInfos[i].replicationController
		replicas := int32(scaleInfos[i].replicas)
		k8Controller.Logger.Printf("Scaling RC %v to %v replicas\n", ctrl.Name, replicas)
		ctrl.Spec.Replicas = &replicas
		_, err = k8Controller.K8Client.UpdateReplicationController(namespace, &ctrl)
		if err != nil {
			k8Controller.Logger.Printf("Error updating RC %v: %v\n", ctrl.Name, err)
			return err
		}
	}

	return nil
}

type scaleInfo struct {
	replicationController v1.ReplicationController
	replicas              int
}

func findReplicationControllerByName(controllers []v1.ReplicationController, name string) (*v1.ReplicationController, error) {
	for _, ctrl := range controllers {
		if ctrl.Name == name {
			return &ctrl, nil
		}
	}
	return nil, errors.New("Replication controller not found: " + name)
}

func (k8Controller *KubernetesController) FindReplicationControllersForApp(namespace, app string) ([]string, error) {
	namespace = getOrDefaultNamespace(namespace)

	rcLabelSelector := map[string]string{"app": app}

	controllers, err := k8Controller.K8Client.ListReplicationControllersWithLabel(namespace, rcLabelSelector)
	if err != nil {
		k8Controller.Logger.Printf("Error finding replication controllers for app %v: %v\n", app, err)
		return []string{}, err
	}

	names := []string{}
	for _, ctrl := range controllers.Items {
		names = append(names, ctrl.Name)
	}

	return names, nil

}

func getOrDefaultNamespace(namespace string) string {
	if namespace != "" {
		return namespace
	} else {
		return "default"
	}
}
