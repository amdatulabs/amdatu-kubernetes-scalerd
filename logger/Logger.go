package logger

import "fmt"

type Logger interface {
	Println(msg ...interface{})
	Print(msg interface{})
	Printf(msg string, args ...interface{})
}

type SysoutLogger struct{}

func (sout SysoutLogger) Println(msg ...interface{}) {
	fmt.Println(msg...)
}

func (sout SysoutLogger) Print(msg interface{}) {
	fmt.Print(msg)
}

func (sout SysoutLogger) Printf(msg string, args ...interface{}) {
	fmt.Printf(msg, args...)
}
